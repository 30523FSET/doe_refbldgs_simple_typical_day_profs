#!/usr/bin/python3
''' Filter raw Eplus CSV outputs to high level hourly only. Do some transform too. '''
import sys
import numpy as np
import pandas as pd
import re
from datetime import datetime


# There is no year data provided in the output. The building models are from 2004. There not a year in
# the weather data either since it's "typical" over some range. Dates need a year though so ... 2004.
year = 2004


def main():
  if len(sys.argv) <= 1:
    print('Usage: %s <csv-files>' % sys.argv[0])
    sys.exit()
  for f in sys.argv[1:]:
    print('File: %s' % (f,))
    print(eplus_energy_l0_p(f).sum())


def eplus_energy_l0_p(f):
  ''' Filter meters down even more to rm gas or fold gas+electricity. Makes use specific assumptions. '''
  (energy_hourly_l0, df) = eplus_energy_l0_j2kWh(f)
  energy_hourly_l0 = energy_hourly_l0.filter(regex='^[^:]+:(Electricity|Gas)$', axis=1)
  energy_hourly_l0['Heating'] = energy_hourly_l0.filter(regex='^Heating:(Electricity|Gas)$', axis=1).sum(axis=1)
  energy_hourly_l0['WaterSystems'] = energy_hourly_l0.filter(regex='^WaterSystems:(Electricity|Gas)$', axis=1).sum(axis=1)
  energy_hourly_l0.drop(
    ['Heating:Electricity', 'Heating:Gas', 'WaterSystems:Gas', 'WaterSystems:Electricity'],
    axis=1, inplace=True, errors='ignore')
  return energy_hourly_l0


def eplus_energy_l0_j2kWh(f):
  ''' Filter down to hourly energy (J) read outs of the first sub category. Also Convert to kWh. '''
  df = eplus_load_csv(f)
  energy_hourly = df.filter(regex='\[J\]\(Hourly\)$', axis=1)
  energy_hourly.columns = list(map(lambda v: v.replace(' [J](Hourly)', ''), energy_hourly.columns.tolist()))
  energy_hourly_l0 = energy_hourly.filter(regex='^[^:]+:(\w+)[^:]*$', axis=1)
  energy_hourly_l0 /= 3600*1e3
  return (energy_hourly_l0, df)


def eplus_load_csv(f):
  df = pd.read_csv(f)
  df['Date/Time'] = df['Date/Time'].apply(lambda s: s.strip())
  df['Date/Time'] = df['Date/Time'].apply(eplus_date_parse)
  df.index = df['Date/Time']
  del df['Date/Time']
  return df


def eplus_date_parse(d):
  ''' Make date a date. Who chose this datetime format? Not supported by strptime!? '''
  tm_eplus_date = list(map(lambda x: int(x), re.match('(\d{2})/(\d{2})\s+(\d{2}):(\d{2}):(\d{2})', d).groups()))
  tm_eplus_date[2] -= 1
  return datetime(year, *tm_eplus_date)


if __name__ == '__main__':
  main()
