#!/bin/bash
files="
RefBldgWarehouseNew2004_Chicago.idf
RefBldgQuickServiceRestaurantNew2004_Chicago.idf
RefBldgFullServiceRestaurantNew2004_Chicago.idf
RefBldgStand-aloneRetailNew2004_Chicago.idf
RefBldgSmallOfficeNew2004_Chicago.idf
RefBldgStripMallNew2004_Chicago.idf
"
# weather_file="USA_IL_Chicago-OHare.Intl.AP.725300_TMY3.epw"
weather_file="AUS_VIC.Tullamarine-Melbourne.Airport.948660_RMY.epw"
out_dir=out/
for i in $files; do
  x=$(echo $i | sed -r 's/\.idf//')
  energyplus -r --output-prefix="${x}_" -i EnergyPlus/Energy+.idd -w "${weather_file}" -d "${out_dir}" $i;
done
rm $(ls "${out_dir}"/* -1 | egrep  -v "_out.csv|_tbl.htm$")  # We don't need this junk - html tables may be useful for overview.
