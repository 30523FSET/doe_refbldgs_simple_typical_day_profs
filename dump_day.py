#!/usr/bin/python3
''' Get typical day of hourly meter readings from pre generated *out.csv simulation outputs.
Metered loads are filtered down to highest order EndUse type and Electricity:Facility is also included.
'''
import sys
import os
import numpy as np
import pandas as pd
import re
from datetime import datetime
import dateutil
import matplotlib.pyplot as plt
from parse_eplus_out_csv import *
from matplotlib.collections import LineCollection


if len(sys.argv) <= 2:
  sys.exit('Usage: %s <YYYY-MM-DD> <csv-files>' % sys.argv[0])
try:
  dateutil.parser.parse(sys.argv[1])
except ValueError:
  sys.exit('Invalid day date')
day = sys.argv[1]
files = sys.argv[2:]
out_dir = ('out_%s' % (day,)).replace('-', '')
if not os.path.isdir(out_dir):
  os.mkdir(out_dir)

for i,f in enumerate(files):
  name = os.path.basename(f).rstrip('.csv') + '_' + day
  print(name, f)
  plt.cla()
  df = eplus_energy_l0_p(f)
  df_day = df[day:day]
  df_day.index = range(24)
  df_day.to_csv('%s/%s.csv' % (out_dir, name,), index_label='Hour')
  for k in df_day:
    plt.plot(df_day[k], label=k)
  plt.title(f)
  plt.legend(
    prop={'size': 8},
    loc='upper right',
    framealpha=0.5,
    frameon=True,
    fancybox=True,
    borderaxespad=0
  )
  plt.xlim(-2,23+2)
  plt.savefig('%s/%s.png' % (out_dir, name,))
