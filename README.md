<style>
</style>
# Overview ##########
This document provides notes on, and results of, an initial investigation into using the [DoE commercial reference buildings (DoECRB)](https://www.energy.gov/eere/buildings/commercial-reference-buildings) as the basis for case studies in day-ahead multi-energy microgrid energy trading and demand response. I'm focusing on using the DoE reference building models provided in the `ExampleFiles/` that come with [EnergyPlus (EPlus)](https://energyplus.net/downloads) (they are later that the versions on the [DoE site](https://www.energy.gov/eere/buildings/commercial-reference-buildings)). It should be noted the reference buildings aren't *actual* buildings but realistic simulations of real buildings.

There is 16 sites in the DoECRB set. I'm focusing on 6 smallest sites in this initial investigation. The goal is to extract hourly or sub-hourly typical daily energy demand (or supply if applicable) profiles for some arbitrary but specified day of a given year for each specified site. Energy profiles should be broken down according to energy type (ex. electricity, gas, geo-thermal) and end use at some level of abstraction (ex. lighting, heating, cooling, appliances). Note, "energy type" and "end use" actually correspond to `EnergyUseType` abd `EndUse` meter labels in EPlus terminology (discussed [below](#Analysis)).

# Summary of Outcomes #########

  - The 6 building model .idf files present have been slightly modified to dump more hourly "meter" data series.
  - The trivial script `run-eplus-simulations.sh` runs energyplus simulation on the 6 building model .idf files present and dumps the results in [out/](out/) - one CSV file for each simulation. Each CSV file contains a year long data series, at hourly to daily intervals, across many data points.
  - The script [dump_day.py](dump_day.py) extracts daily load profiles by major `EndUse` type, for a specified day of the year (2004) and dumps them in a directory `out_<date-ofday>/`. Note this script makes some context specific manipulations to the data:
    - Data is filtered down to hourly `EndUse` only data series.
    - `WaterSystems:Gas|Electricity` and `Heating:Gas|Electricity` are combined into one data series under the assumption `Electricity` and `Gas` are substitutes.
    - `InteriorEquipment:Gas`, which if present, is for cooking and can't be reasonable substituted for `Electricity` and is left as is.

Figures below show load profile by EndUse for 30/09/2004 for the 6 sites. Raw data is in [out_20040930/](out_20040930/):

---

<table style="width: 100%">
  <tr>
    <td>
      <figure id='f1'>
        <img style="max-width: 360px" src="./out_20040930/RefBldgFullServiceRestaurantNew2004_Chicago_out_2004-09-30.png"><br/>
      </figure>
    </td>
    <td>
      <figure id='f2'>
        <img style="max-width: 360px" src="./out_20040930/RefBldgQuickServiceRestaurantNew2004_Chicago_out_2004-09-30.png"><br/>
      </figure>
    </td>
    <td>
      <figure id='f3'>
        <img style="max-width: 360px" src="./out_20040930/RefBldgSmallOfficeNew2004_Chicago_out_2004-09-30.png"><br/>
      </figure>
    </td>
  </tr>
  <tr>
    <td>
      <figure id='f4'>
        <img style="max-width: 360px" src="./out_20040930/RefBldgStandAloneRetailNew2004_Chicago_out_2004-09-30.png"><br/>
      </figure>
    </td>
    <td>
      <figure id='f5'>
        <img style="max-width: 360px" src="./out_20040930/RefBldgStripMallNew2004_Chicago_out_2004-09-30.png"><br/>
      </figure>
    </td>
    <td>
      <figure id='f6'>
        <img style="max-width: 360px" src="./out_20040930/RefBldgWarehouseNew2004_Chicago_out_2004-09-30.png"><br/>
      </figure>
    </td>
  </tr>
</table>

---

# Analysis ##########
TL;DR: This section contains notes on how EnergyPlus works and how the above daily profiles were derived.

Running EnergyPlus simulations on a given reference building file with an appropriate config, gives a CSV dump of a bunch of "Meter" outputs at hourly resolution (see [out/*.csv](out/)). There is two (main?) types of Meter output:

  - `EndUse`; What energy is used for. Ex `Refrigeration`, `InteriorEquipment`.
  - `EnergyUseType`; What form energy takes. Ex `Electricity`, `Gas`, `EnergyTransfer`, `Cogeneration`.

We can use either set to get complementary views of the energy consumption of a building (note the whole building/site/model is called the "Facility" in the modeling language). `EndUse` meter readings typically also include a `EnergyUseType` type tag (example `Refrigeration:Electricity`), but it seems `EnergyUseType` series don't provide a breakdown across `EndUse` but rather by building area (example `Electricity:Facility`), at least in the DoECRB models as currently configured. Meter readings are tagged hierarchically to 2 or more levels in some cases. However, I've limited scope to first sub-divisions of `EndUse` and `EndUseType` meter outputs. The `EndUse` categorization is most relevant, and I'm mostly ignoring `EnergyUseType`. `Facility:Electricity` from `EnergyUseType` could provide a sanity check (all `<EndUse>:Electricity` should sum to `Facility:Electricity`). Other `EnergyUseType` meters such as `Facility:Gas` aren't much use to me.

Across all 6 sites, the full set of meter outputs to the first level is:

    [ 'Cogeneration:ElectricityNet',
      'Cogeneration:ElectricityPurchased',
      'Cogeneration:ElectricitySurplusSold',
      'Cooling:Electricity',
      'Cooling:EnergyTransfer',
      'CoolingCoils:EnergyTransfer',
      'ElectricEmissions:Source',
      'Electricity:Building',
      'Electricity:Facility',
      'Electricity:HVAC',
      'Electricity:Plant',
      'ElectricityNet:Facility',
      'ElectricityNet:Plant',
      'ElectricityPurchased:Facility',
      'ElectricityPurchased:Plant',
      'ElectricitySurplusSold:Facility',
      'ElectricitySurplusSold:Plant',
      'EnergyTransfer:Building',
      'EnergyTransfer:Facility',
      'EnergyTransfer:HVAC',
      'EnergyTransfer:Plant',
      'ExteriorLights:Electricity',
      'Fans:Electricity',
      'Gas:Building',
      'Gas:Facility',
      'Gas:HVAC',
      'Gas:Plant',
      'Heating:Electricity',
      'Heating:EnergyTransfer',
      'Heating:Gas',
      'HeatingCoils:EnergyTransfer',
      'InteriorEquipment:Electricity',
      'InteriorEquipment:Gas',
      'InteriorLights:Electricity',
      'NaturalGasEmissions:Source',
      'PlantLoopHeatingDemand:Facility',
      'PlantLoopHeatingDemand:Plant',
      'Pumps:Electricity',
      'PurchasedElectricEmissions:Source',
      'Refrigeration:Electricity',
      'Refrigeration:EnergyTransfer',
      'SoldElectricEmissions:Source',
      'Source:Facility',
      'WaterSystems:EnergyTransfer',
      'WaterSystems:Gas',
      'WaterSystems:PlantLoopHeatingDemand']

However, across the all 6 sites the only applicable energy types are `Electricity` and `Gas` (and some site don't have gas). Limiting to `EndUse` of `Electricity` or `Gas` we get:

    [ 'Cooling:Electricity',
      'ExteriorLights:Electricity',
      'Fans:Electricity',
      'Heating:Electricity',
      'Heating:Gas',
      'InteriorEquipment:Electricity',
      'InteriorEquipment:Gas',
      'InteriorLights:Electricity',
      'Pumps:Electricity',
      'Refrigeration:Electricity',
      'WaterSystems:Gas']

`InteriorEquipment:Gas` occurs on Restaurants and probably can't be substituted with Electricity. It is very likely an inelastic energy use anyway. The other `Gas` loads can probably be serviced via electricity (given technology exists ...).

So we have some fixed hourly load profiles of high level energy usages. The EnergyPlus models don’t provide any indications about flexibility (i.e. price elasticity). So ... let's make some shit up, while being aware there is essentially arbitrarily many possibilities we could come up with. What is reasonable can only be determined on a case by case basis.

First up, *we* only want to deal with district heating and electricity. Where `WaterSystems` and `Heating` may be serviced by district heating or electricity. Combining those and ignoring `InteriorEquipment:Gas` as an externality we have:

    [ 'Cooling:Electricity',
      'ExteriorLights:Electricity',
      'Fans:Electricity',
      'Heating',
      'InteriorEquipment:Electricity',
      'InteriorLights:Electricity',
      'Pumps:Electricity',
      'Refrigeration:Electricity',
      'WaterSystems']

Now for the sake of simulating "what ifs", we impose various flexibility (i.e. price elasticity) semantics across the various loads. A load is either flexible or it is fixed. If flexible, a load's flexibility is described by a type, and type specific parameters which encode the level of flexibility at various times. Supposing the following type of flexibility exist:

  - Fixed: Not flexible at all.
  - Scalable: Means the amount of energy required at any given interval can be scaled back to a degree and need not be recovered later. Example: dimmable lighting.
  - Shiftable: The total energy required is fixed (is possibly scalable too) but when the energy is consumed can be shifted from one time to another.
  - Thermal: Means the set point temperature at any given interval can be scaled back to a degree and *the set point* need not be recovered later. Note however that there is a dependency between temperature at different times making energy demand servicing thermal loads both shiftable and scalable in nature. To a level of fidelity one could also model a Thermal load as just a Shiftable load, making an implicit assumption thermal storage exists or the load itself is a thermal mass.

A reasonable mapping to flexibility types might be:

  - Cooling:Electricity; Thermal
  - ExteriorLights:Electricity; Scalable
  - Fans:Electricity; Fixed
  - Heating; Thermal
  - InteriorEquipment:Electricity; Fixed
  - InteriorLights:Electricity: Scalable|Fixed
  - Pumps:Electricity: Shiftable|Fixed
  - Refrigeration:Electricity: Thermal|Fixed
  - WaterSystems: Thermal

Now we attach various TES, EES, CoGen, PV, ... and do analysis and stuff ..
